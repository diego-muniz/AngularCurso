# PrimeiroProjeto
-Primeiro

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests
 
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Obs.:

### install

install `node`

npm install -g `typescript`

npm install -g `@angular/cli`

ng-v -> Verify version `Angular/cli`

### Create project
ng new <!name project>

### Create Component
Exemplo .: ng g c meu-primeiro

ng g c `<!name project>`

`g` -> Generat

`c` -> Component

### Create Module

ng g m `name module`

### Start Application

`ng serve`

#### After successful

`webpack: Compiled successfully`

Port Default: `localhost:4200`


### Working How Modules.

Criar componente isolado para um determinado module.

Criar Module. `ng g m curso`.

Criar componente para esse module `ng g c cursos/curso-detalhe`.

Adicione CursoDetalheComponent em Declarations - cursos.module.ts

### Create service

`ng g s <!name Service>`

Após criar o service, navegue até o arquivo de module.

cursos.module.ts -> NgModule, adicione o provide do service.
```html
@NgModule({
  providers: [ CursosService ]
})
```

### Export/Import

`Atenção: Exposrts o Component, Import importa o Module`
 ```html
   exports: [MeuFormComponent]

  export class MeuFormComponent implements OnInit - Arquivo meu-form.component.ts

  import { MeuFormModule } from './../meu-form/meu-form.module';

  imports: {
    MeuFormModule
  }

  ```




