import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DataBindingComponent } from './data-binding/data-binding.component';
import { MeuFormModule } from './../meu-form/meu-form.module';
import { InputPropertyModule } from '../input-property/input-property.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MeuFormModule,
    InputPropertyModule
  ],
  declarations: [
    DataBindingComponent
  ],
  exports: [
    DataBindingComponent
  ],

})
export class DataBindingModule { }

