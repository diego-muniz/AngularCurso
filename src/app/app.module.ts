// Prepara aplicação para browser
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { MeuPrimeiroComponent} from './meu-primeiro/meu-primeiro.component';
import { MeuPrimeiro2Component } from './meu-primeiro2/meu-primeiro2.component';
import { CursosModule } from './cursos/cursos.module';
import { DataBindingModule } from './data-binding/data-binding.module';
import { UsuarioModule } from './usuario/usuario.module';
// import { Ng2InputMaskModule } from 'ng2-input-mask';

// Decoration Class

@NgModule({
  declarations: [
    // Declarations Componentes, Directivas e Pipes
    AppComponent,
    MeuPrimeiroComponent,
    MeuPrimeiro2Component
  ],
  imports: [
    BrowserModule,
    CursosModule,
    DataBindingModule,
    UsuarioModule
  ],
  // Serviços que vão ficar disponivel para todos
  // Scope Global
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
