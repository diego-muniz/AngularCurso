import { CursosService } from './cursos.service';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CursosComponent } from './cursos.component';
import { CursoDetalheComponent } from './curso-detalhe/curso-detalhe.component';

// Module de funcionalidade não importa o BowerModule,
// importa CommonModule
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    CursosComponent,
    CursoDetalheComponent
  ],
  exports: [
    CursosComponent
  ],
  // Autenticar quem vai me retornar o servico
  providers: [ CursosService ]
})
export class CursosModule { }
