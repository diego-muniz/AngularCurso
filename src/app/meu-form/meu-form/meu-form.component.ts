import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-meu-form',
  templateUrl: './meu-form.component.html',
  styleUrls: ['./meu-form.component.css']
})
export class MeuFormComponent implements OnInit {

  url: string;
  urlImg: string;
  valorAtual: string;
  salvarValorAtual: string;
  salvarValorBlur: string;
  isMouseOver: boolean;

  nome: string;
  pessoa: any = {
    nome: 'Nome pessoa',
    idade: 20,
    endereco: {
      rua: 'casa',
      casa: 5,
      cidade: 'Brasilia'
    }
  };

  constructor() {
  this.isMouseOver = false;
  this.nome = 'teste';
  this.url = 'www.google.com.br';
  this.urlImg = 'http://lorempixel.com/400/200/sports/1';
 }

 botaoClicado() {
  alert('Botão Clicado');
}

onKeyUp(evento: KeyboardEvent) {
  this.valorAtual = (<HTMLInputElement>evento.target).value;
}

salvarValor(valor) {
  this.salvarValorAtual = valor;
}

salvarValorB(valor) {
  this.salvarValorAtual = valor;
}


onMouseOverOut() {
  this.isMouseOver = !this.isMouseOver;
}
  ngOnInit() {
  }

}
