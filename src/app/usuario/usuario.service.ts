import { Injectable } from '@angular/core';

@Injectable()
export class UsuarioService {

  constructor() { }

  pessoa: any = {
    nome: 'Nome pessoa',
    idade: 20,
    endereco: {
      rua: 'casa',
      casa: 5,
      cidade: 'Brasilia'
    }
  };

  usuarios: any = [
    {
      id: 1,
      nome: 'Diego',
      login: 'diego.muniz',
      sobreNome: 'Alves',
      email: 'diego.muniz@infocosolutions.com.br',
      telefone: '(61) 94781-4577',
      telefoneComercial: '(61) 3333-4577',
      departamentoDoUsuario: 'departamentoDoUsuario',
      empresa: 'In foco Solutions'
    },
    {
      id: 2,
      nome: 'Gabriel',
      login: 'gabriel.fernando',
      sobreNome: 'Fernando',
      email: 'gabriel.fernando@infocosolutions.com.br',
      telefone: '(61) 84781-4227',
      telefoneComercial: '(61) 3333-5577',
      departamentoDoUsuario: 'departamentoDoUsuario',
      empresa: 'In foco Solutions',

    }];


  getUsuario() {
    return this.usuarios;
  }


}
