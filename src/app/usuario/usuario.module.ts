import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuarioPerfilComponent } from './usuario-perfil/usuario-perfil.component';
import { UsuarioNovoComponent } from './usuario-novo/usuario-novo.component';
import { UsuarioListarComponent } from './usuario-listar/usuario-listar.component';
import { UsuarioService } from './usuario.service';
import { ModalModule } from 'ngx-modialog';
import { BootstrapModalModule } from 'ngx-modialog/plugins/bootstrap';

@NgModule({
  imports: [
    CommonModule,
    ModalModule.forRoot(),
    BootstrapModalModule
  ],
  declarations: [UsuarioPerfilComponent, UsuarioNovoComponent, UsuarioListarComponent],
  exports: [
    UsuarioPerfilComponent,
    UsuarioNovoComponent,
    UsuarioListarComponent
  ],
  providers: [UsuarioService]
})
export class UsuarioModule { }
