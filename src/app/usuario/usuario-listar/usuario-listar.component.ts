import { Component, OnInit } from '@angular/core';
import { UsuarioService } from './../usuario.service';
import { Overlay } from 'ngx-modialog';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

@Component({
  selector: 'app-usuario-listar',
  templateUrl: './usuario-listar.component.html',
  styleUrls: ['./usuario-listar.component.css']
})
export class UsuarioListarComponent implements OnInit {

  // usuarios: string[];

  usuarios: any = {
        id : 0,
        nome : '',
        login: '',
        sobreNome : '',
        email : '',
        telefone : '',
        telefoneComercial: '',
        departamentoDoUsuario: '',
        empresa: ''
      };

  constructor(private usuarioService: UsuarioService, public modal: Modal) {

    this.usuarios = this.usuarioService.getUsuario();

   }

   detalheUsuario(usuario) {
    const dialogRef = this.modal.alert()
        .size('lg')
        .showClose(true)
        .title('Detalhe Usuário')
        .body(`
            <b>Nome: ${usuario.nome} ${usuario.sobreNome}</b>
            <ul>
                <li>Login: ${usuario.login}</li>
                <li>E-mail: ${usuario.email}</li>
                <li>Telefone: ${usuario.telefone}</li>
                <li>Telefone Comercial: ${usuario.telefoneComercial}</li>
                <li>Departamento do Usuário: ${usuario.departamentoDoUsuario}</li>
                <li>Empresa: ${usuario.empresa}</li>
            </ul>`)
        .open();

    dialogRef.result
        .then( result => close() );
   }

  ngOnInit() {
  }

}
