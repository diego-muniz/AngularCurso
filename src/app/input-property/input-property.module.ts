import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputPropertyComponent } from './input-property/input-property.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [InputPropertyComponent],
  exports: [
    InputPropertyComponent
  ]
})
export class InputPropertyModule { }
